#!/usr/bin/env bash

set -e
set -x

echo "Building for $BUILD_TARGET"

export BUILD_PATH=$UNITY_DIR/Builds/$BUILD_TARGET/
mkdir -p $BUILD_PATH
echo "$UNITY_LICENSE" > ./Unity_license.ulf

# activate the editor
# unity-editor -batchmode -manualLicenseFile ./Unity_license.ulf -logfile /dev/stdout
cat ./Unity_license.ulf

# Chech what is the build for player
# see : https://docs.unity3d.com/Manual/EditorCommandLineArguments.html
# it should be value of Build Arguments -> buildTarget 
# • Standalone
# • Win
# • Win64
# • OSXUniversal
# • Linux64
# • iOS
# • Android
# • WebGL
# • WindowsStoreApps
# • tvOS


# Availabe values 
# StandaloneWindows64, StandaloneLinux64
BUILCMD=""
# Use a case statement to execute different code blocks based on the value of the variable
case $BUILD_TARGET in
  "StandaloneLinux64")
    BUILCMD="-buildLinux64Player $BUILD_PATH/$BUILD_NAME"
    ;;
  "StandaloneOSX")
    BUILCMD="-buildOSXUniversalPlayer $BUILD_PATH/$BUILD_NAME.app"
    ;;
  "StandaloneWindows64")
    BUILCMD="-buildWindows64Player $BUILD_PATH/$BUILD_NAME.exe"
    ;;
  "WebGL")
    BUILCMD="-buildTarget $BUILD_TARGET \
      -customBuildTarget $BUILD_TARGET \
      -customBuildName $BUILD_NAME \
      -customBuildPath $BUILD_PATH \
      -executeMethod BuildCommand.PerformBuild"
    ;;
  "Android")
    BUILCMD="-buildTarget $BUILD_TARGET \
      -customBuildTarget $BUILD_TARGET \
      -customBuildName $BUILD_NAME \
      -customBuildPath $BUILD_PATH \
      -executeMethod BuildCommand.PerformBuild"
  ;;
  "iOS")
      BUILCMD="-buildTarget $BUILD_TARGET \
      -customBuildTarget $BUILD_TARGET \
      -customBuildName $BUILD_NAME \
      -customBuildPath $BUILD_PATH \
      -executeMethod BuildCommand.PerformBuild"
    ;;
  *)
    echo "Invalid BUILD_TARGET is set"
    exit 1
    # Add code to execute for invalid options
    ;;
esac


unity-editor \
  -batchmode \
  -quit \
  $BUILCMD \
  -projectPath $UNITY_DIR \
  -logFile /dev/stdout

UNITY_EXIT_CODE=$?

if [ $UNITY_EXIT_CODE -eq 0 ]; then
  echo "Run succeeded, no failures occurred";
elif [ $UNITY_EXIT_CODE -eq 2 ]; then
  echo "Run succeeded, some tests failed";
elif [ $UNITY_EXIT_CODE -eq 3 ]; then
  echo "Run failure (other failure)";
else
  echo "Unexpected exit code $UNITY_EXIT_CODE";
fi

# ls -l
# echo "-----------------"
# ls -la $BUILD_PATH
# echo "-----------------"
# ls -lh
# echo "-----------------"
[ -n "$(ls -A $BUILD_PATH)" ] # fail job if build folder is empty
